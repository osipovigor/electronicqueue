﻿$(document).ready(function() {
    initEvents();
});

function initEvents() {
    var id = '';
    $(document).on('click', "a.changeLectureHallBtn", function (e) {
        id = this.parentElement.parentElement.firstElementChild.innerHTML;
    });

    $('#changeLectureHallBtn').click(function (e) {
        e.preventDefault();
        var data = {
            id: id,
            lectureHallId: $('#ChangeLectureHallLectureHalls').val(),
            specialtyId: $('#ChangeLectureHallSpecialties').val()
        };
        $.ajax({
            url: 'ControlPanel/ChangeLectureHall',
            type: 'POST',
            data: data,
            success: function(data) {
                location.reload();
            },
            error: function(resp) {
                location.reload();
            }
        });
    });

    //показать модальное окно
    $('#showModalUserAdding').click(function(e) {
        e.preventDefault();
        $('#addUserModal').modal('show');
    });

    $('.changeSpecialtyBtn').click(function(e) {
        e.preventDefault();
        id = $(this).data('id');
        $('#changeSpecialtyModal').modal('show');
        //id = this.parentElement.parentElement.firstElementChild.innerHTML;
    });


    $('#changeSpecialtyModalBtn').click(function(e) {
        e.preventDefault();
        var data = {
            id: id,
            name: $('#SpecialtyName').val(),
            userId: $('#TeacherId').val()
        };

        $.ajax({
            url: 'ControlPanel/ChangeSpecialty',
            type: 'POST',
            data: data,
            success: function (data) {
                location.reload();
            },
            error: function (resp) {
                location.reload();
            }
        });

    });

    $('#addUser').click(function(e) {
        e.preventDefault();
        var data = {
            login: $('#login').val(),
            password: $('#password').val(),
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val(),
            roleId: $('#role').val()
        };
        $.ajax({
            url: window.urls.addUser,
            type: 'POST',
            data: data,
            success: function(data) {
                location.reload();
            },
            error: function (resp) {
                var errorSpan = $('<span />').addClass('text-danger').html(resp.responseText);
                $('#addingUserErrors').html(errorSpan);
            }
        });
    });
}