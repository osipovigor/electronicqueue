﻿$(document).ready(function () {
    checkCurrentRequest();
    initEvents();
});

function initEvents() {
    $('#next').on('click', function() {
        sendPostRequest(false);
    });

    $('#apply').on('click', function() {
        closeOrApplyRequest(false);
    });

    $('#late').on('click', function() {
        closeOrApplyRequest(true);
    });
}

function closeOrApplyRequest(isClosed) {
    $.post('Reception/ApplyRequest', { requestNumber: $('#requestNumber').text(), isClosed: isClosed }, function () {
        location.reload();
    });
}

function checkCurrentRequest() {
    sendPostRequest(true);
}

function sendPostRequest(isChecked) {
    $.post('Reception/InviteEnrollee', { isChecked: isChecked }, function (data) {
        $('#requestNumber').text(data.RequestNumber);
        $('#enrolleeName').text(data.EnrolleeName);
        $('#registrationDate').text(data.RegistrationDateString);
        $('#specialty').text(data.SpecialtyName);

        $('#enrolleeInfo').show();
        if (data.IsBusy == true)
            $('#enrolleeInfo span').show();
        else 
            $('#enrolleeInfo span').hide();

        $('#next').attr('disabled', true);
        $('#late').attr('disabled', false);
        $('#apply').attr('disabled', false);
    }).fail(function (data) {
        var result = data.responseJSON;
        if (result.isError == true)
            alert(result.error);
    });
}