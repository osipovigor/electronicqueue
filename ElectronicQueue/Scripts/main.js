﻿$(document).ready(function () {
    initEvents();
    getSpecialties();
});

function initEvents() {
    $('#registerEnrollee').click(function() {
        $.ajax({
            url: '/enrolleeRegistration/register',
            type: 'POST',
            data: getEnrolleeData(),
            success: function(number) {
                $('body').bsMsgBox({
                    title: "Номер заявки",
                    text: "<h1>" + number + "</h1>"
                });
            }
        });
    });
}

function getEnrolleeData() {
    var data = {};
    data.FirstName = $('#EnrolleeName').val();
    data.LastName = $('#EnrolleeLastName').val();
    data.SpecialtyId = $('#specialty_list').val();
    return data;
}

function getSpecialties() {
    $.ajax({
        url: '/Home/GetSpecialties',
        type: 'POST',
        success: function (data) {
            var options = '';
            $.each(data, function(index, value) {
                options += '<option value="' + value.SpecialtyId + '">' + value.Name + '</option>';
            });
            var specialtySelect = document.getElementById('specialty_list');
            specialtySelect.innerHTML = options;

        }
    });
}

