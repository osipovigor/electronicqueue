﻿using System.Web.Security;
using ElectronicQueue.App_Start;
using ElectronicQueue.BL.Repositories;
using ElectronicQueue.Data.Entities;
using Ninject;

namespace ElectronicQueue.Providers
{
    public class CustomRoleProvider : RoleProvider
    {
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;

        public CustomRoleProvider()
        {
            IKernel kernel = NinjectWebCommon.Kernel;

            _roleRepository = kernel.Get<IRoleRepository>();
            _userRepository = kernel.Get<IUserRepository>();

        }

        public override string[] GetRolesForUser(string username)
        {
            string[] role = { };
            try
            {
                User user = _userRepository.GetUser(username);
                if (user != null)
                {
                    Role userRole = _roleRepository.GetRole(user.RoleId);
                    if (userRole != null)
                        role = new[] { userRole.RoleName };
                }

            }
            catch
            {
                role = new string[] { };
            }

            return role;
        }

        public override void CreateRole(string roleName)
        {
            Role newRole = new Role { RoleName = roleName };
            _roleRepository.AddRole(newRole);
            _roleRepository.SaveChanges();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            bool outputResult = false;

            try
            {
                User user = _userRepository.GetUser(username);
                if (user != null)
                {
                    Role userRole = _roleRepository.GetRole(user.RoleId);
                    if (userRole != null && userRole.RoleName == roleName)
                        outputResult = true;
                }
            }
            catch
            {
                outputResult = false;
            }

            return outputResult;
        }


        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new System.NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new System.NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new System.NotImplementedException();
        }

        public override string ApplicationName { get; set; }
    }
}