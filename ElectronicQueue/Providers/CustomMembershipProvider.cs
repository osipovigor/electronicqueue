﻿using System;
using System.Web.Security;
using ElectronicQueue.App_Start;
using ElectronicQueue.BL.Repositories;
using ElectronicQueue.Data.Entities;
using Ninject;

namespace ElectronicQueue.Providers
{
    public class CustomMembershipProvider : MembershipProvider
    {
        private readonly IUserRepository _userRepository;

        public CustomMembershipProvider()
        {
            IKernel kernel = NinjectWebCommon.Kernel;

            _userRepository = kernel.Get<IUserRepository>();
        }


        public override bool ValidateUser(string username, string password)
        {

            bool isValid = false;

            try
            {
                User user = _userRepository.GetUser(username);

                if (user != null && user.Password == password)
                    isValid = true;
            }
            catch(Exception e)
            {
                isValid = false;
            }
            return isValid;
        }

        public MembershipUser CreateUser(string username, string password)
        {
            MembershipUser membershipUser = GetUser(username, false);

            if (membershipUser == null)
            {
                try
                {
                    User user = new User
                    {
                        Login = username,
                        Password = password
                    };
                    _userRepository.AddUser(user);
                    _userRepository.SaveChanges();
                    membershipUser = GetUser(username, false);
                    return membershipUser;

                }
                catch (Exception)
                {
                    return null;
                }
            }
            return null;
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            try
            {
                User user = _userRepository.GetUser(username);
                MembershipUser membershipUser = new MembershipUser("CustomMembershipProvider", user.Login, null, null, null,
                    null, false, false, DateTime.Now, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue,
                    DateTime.MinValue);

                return membershipUser;
            }
            catch (Exception)
            {
                return null;
            }
        }

        #region unused methods

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer,
            bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }
        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion,
            string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override string ApplicationName { get; set; }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}