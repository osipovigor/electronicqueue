﻿using System;
using System.Web.Mvc;
using ElectronicQueue.BL.Enums;
using ElectronicQueue.BL.Repositories;
using ElectronicQueue.Data.Entities;
using ElectronicQueue.Models;

namespace ElectronicQueue.Controllers
{
    [Authorize(Roles = "operator")]
    public class EnrolleeRegistrationController : Controller
    {
        private readonly IRequestRepository _requestRepository;
        private readonly ILectureHallRepository _lectureHallRepository;

        public EnrolleeRegistrationController(IRequestRepository requestRepository, ILectureHallRepository lectureHallRepository)
        {
            _requestRepository = requestRepository;
            _lectureHallRepository = lectureHallRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public string Register(EnrolleeRegisterModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.FirstName) || string.IsNullOrEmpty(model.LastName))
                    return "Необходимо заполнить имя и фамилию";
                Enrollee enrollee = new Enrollee();
                enrollee.FirstName = model.FirstName;
                enrollee.LastName = model.LastName;
                enrollee.CreationDate = DateTime.Now;

                //регистрируем абитуриента
                _requestRepository.RegisterEnrollee(enrollee);
                _requestRepository.SaveChanges();


                Request request = new Request();
                request.EnrolleeId = enrollee.EnrolleeId;
                request.RequestNumber = _requestRepository.GetRequestNumber();
                request.SpecialtyId = Guid.Parse(model.SpecialtyId);
                request.StepId = (int) RequestStep.New;

                //выбираем первый
                request.LectureHallTableId = _lectureHallRepository.GetRandomTableBySpeсialty(request.SpecialtyId);
                request.CreationDate = DateTime.Now;
                //создаем заявку
                _requestRepository.CreateRequest(request);
                _requestRepository.SaveChanges();
                return request.RequestNumber.ToString();
            }
            catch (Exception e)
            {

                return e.Message;
            }
            
        }
    }
}