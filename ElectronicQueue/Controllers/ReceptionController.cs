﻿using System;
using System.Web.Mvc;
using ElectronicQueue.BL.Models;
using ElectronicQueue.BL.Repositories;

namespace ElectronicQueue.Controllers
{
    [Authorize(Roles = "teacher")]
    public class ReceptionController : Controller
    {
        private readonly IRequestRepository _requestRepository;
        private readonly ISpecialtyRepository _specialtyRepository;

        public ReceptionController(IRequestRepository requestRepository, ISpecialtyRepository specialtyRepository)
        {
            _requestRepository = requestRepository;
            _specialtyRepository = specialtyRepository;
        }

        public ActionResult Index()
        {
            ViewBag.SpecialtyNames = _specialtyRepository.GetSpecialtyNames(User.Identity.Name);
            return View();
        }

        [HttpPost]
        public ActionResult InviteEnrollee(bool isChecked)
        {
            try
            {
                string userName = User.Identity.Name;
                EnrolleeViewModel request = _requestRepository.GetTopRequest(userName, isChecked);
                
                return Json(request);
            }
            catch (Exception e)
            {
                Response.StatusCode = 404;
                return Json(new { isError = true, error = e.Message });
            }
            
        }

        public ActionResult ApplyRequest(int requestNumber, bool isClosed)
        {
            _requestRepository.ApplyRequest(requestNumber, isClosed);
            _requestRepository.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}