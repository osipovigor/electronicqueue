﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ElectronicQueue.App_Start;
using ElectronicQueue.BL.Repositories;
using ElectronicQueue.Data.Entities;
using Ninject;

namespace ElectronicQueue.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ISpecialtyRepository _specialtyRepository;

        public HomeController(ISpecialtyRepository specialtyRepository )
        {
            _specialtyRepository = specialtyRepository;
        }


        public ActionResult Index()
        {
           
            return View();
        }

        public JsonResult GetSpecialties()
        {
            List<Specialty> specs = _specialtyRepository.GetAll();

            return Json(specs.Select(x => new
            {
                x.SpecialtyId,
                x.Name
            }));
        }
    }
}