﻿using System.Web.Mvc;
using System.Web.Security;
using ElectronicQueue.Models;

namespace ElectronicQueue.Controllers
{
    [AllowAnonymous]
    public class AuthorizationController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel loginInfo, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(loginInfo.Login, loginInfo.Password))
                {
                    FormsAuthentication.SetAuthCookie(loginInfo.Login, loginInfo.RememberMe);
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Неправильный пароль или логин");
            }



            return View("Index");
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }
    }
}