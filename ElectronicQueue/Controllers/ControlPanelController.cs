﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ElectronicQueue.BL.Repositories;
using ElectronicQueue.Data.Entities;

namespace ElectronicQueue.Controllers
{
    [Authorize(Roles = "admin")]
    public class ControlPanelController : Controller
    {
        private readonly ISpecialtyRepository _specialtyRepository;
        private readonly ILectureHallRepository _lectureHallRepository;
        private readonly IUserRepository _userRepository;

        public ControlPanelController(ISpecialtyRepository specialtyRepository, ILectureHallRepository lectureHallRepository, IUserRepository userRepository)
        {
            _specialtyRepository = specialtyRepository;
            _lectureHallRepository = lectureHallRepository;
            _userRepository = userRepository;
        }

        private void GetViewBagData()
        {
            ViewBag.LectureHallTables = _lectureHallRepository.GetAll();

            List<LectureHall> lectureHalls = _lectureHallRepository.GetLectureHalls();
            ViewBag.LectureHalls = lectureHalls.Select(x => new SelectListItem {Text = x.Name, Value = x.LectureHallId.ToString()});

            List<Specialty> specialties = _specialtyRepository.GetAll();
            ViewBag.Specialties = specialties.Select(x => new SelectListItem { Text = x.Name, Value = x.SpecialtyId.ToString() });

            List<User> teachers = _userRepository.GetTeachers();
            ViewBag.Teachers = teachers.Select(x => new SelectListItem() {Text = x.Login, Value = x.UserId.ToString()});

            ViewBag.Users = _userRepository.GetUsers();

        }
        public ActionResult Index()
        {
            GetViewBagData();
            return View(_specialtyRepository.GetSpecialtiesForControlPanel());
        }

        public ActionResult Delete(string guid)
        {
            _specialtyRepository.Delete(guid);
            _specialtyRepository.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Add(string name, string teacher, bool isPrior)
        {
            if (string.IsNullOrEmpty(name))
                return RedirectToAction("Index");
            _specialtyRepository.Add(name, isPrior, teacher);
            _specialtyRepository.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult ChangeSpecialty(string id, string name, string userId)
        {
            _specialtyRepository.ChangeSpecialty(id, name, userId);
            _specialtyRepository.SaveChanges();
            return RedirectToAction("Index");
        }

        public void ChangeLectureHall(string id, int? lectureHallId, string specialtyId)
        {
            _lectureHallRepository.ChangeLectureHall(Guid.Parse(id), lectureHallId, Guid.Parse(specialtyId));
            _lectureHallRepository.SaveChanges();

        }

        [HttpPost]
        public ActionResult AddLectureHallTable(string name, int lectureHallId, string specialtyId)
        {
            _lectureHallRepository.AddLectureHallTable(name, lectureHallId, Guid.Parse(specialtyId));
            _lectureHallRepository.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult DeleteLectureTable(string guid)
        {
            _lectureHallRepository.DeleteTable(Guid.Parse(guid));
            _lectureHallRepository.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult DeleteUser(string guid)
        {
            _userRepository.DeleteUser(Guid.Parse(guid));
            _userRepository.SaveChanges();
            return RedirectToAction("Index");
        }

        public string AddUser(User user)
        {
            try
            {
                if (string.IsNullOrEmpty(user.Login) || string.IsNullOrEmpty(user.Password))
                    throw new Exception("Логин и пароль обязательны для заполнения");
                _userRepository.AddUser(user);
                _userRepository.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                Response.StatusCode = 404;
                return e.Message;
            }
            
            
        }
    }
}