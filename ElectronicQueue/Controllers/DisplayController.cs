﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ElectronicQueue.BL.Enums;
using ElectronicQueue.BL.Models;
using ElectronicQueue.BL.Repositories;
using ElectronicQueue.Data.Entities;

namespace ElectronicQueue.Controllers
{
    public class DisplayController : Controller
    {
        private readonly IRequestRepository _requestRepository;

        public DisplayController(IRequestRepository requestRepository)
        {
            _requestRepository = requestRepository;
        }

        public ActionResult Index()
        {
            List<RequestViewModel> requests = _requestRepository.GetRequests();
            if (!User.IsInRole("admin"))
                requests = requests.Where(x => x.StepId == RequestStep.New).ToList();
            return View(requests);
        }

        [Authorize(Roles = "admin")]
        public ActionResult DeleteRequest(string requestId)
        {
            _requestRepository.DeleteRequest(Guid.Parse(requestId));
            _requestRepository.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}