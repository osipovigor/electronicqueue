﻿namespace ElectronicQueue.Models
{
    public class EnrolleeRegisterModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string SpecialtyId { get; set; }
    }
}