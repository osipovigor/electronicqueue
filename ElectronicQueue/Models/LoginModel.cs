﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ElectronicQueue.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Введите логин")]
        [DisplayName("Логин")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Введите пароль")]
        [DisplayName("Пароль")]
        public string Password { get; set; }

        [DisplayName("Запомнить?")]
        public bool RememberMe { get; set; }
    }
}