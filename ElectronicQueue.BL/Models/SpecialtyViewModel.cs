﻿namespace ElectronicQueue.BL.Models
{
    public class SpecialtyViewModel
    {
        public string SpecialtyId { get; set; }

        public string Name { get; set; }
        
        public string Teacher { get; set; }
    }
}
