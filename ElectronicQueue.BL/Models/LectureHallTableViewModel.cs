﻿namespace ElectronicQueue.BL.Models
{
    public class LectureHallTableViewModel
    {
        public string Id{ get; set; }

        public string Name { get; set; }

        public string LectureHallName { get; set; }

        public string SpecialtyName { get; set; }
    }
}
