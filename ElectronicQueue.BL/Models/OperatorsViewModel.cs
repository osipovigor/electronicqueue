﻿using System;

namespace ElectronicQueue.BL.Models
{
    public class OperatorsViewModel
    {
        public Guid UserId { get; set; }
        public string Login { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string RoleName { get; set; }
    }
}
