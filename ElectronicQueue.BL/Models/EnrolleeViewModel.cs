﻿using System;

namespace ElectronicQueue.BL.Models
{
    /// <summary>
    /// Модель для отображения заявок
    /// </summary>
    public class EnrolleeViewModel
    {
        public int RequestNumber { get; set; }

        public DateTime RegistrationDate { get; set; }

        public string RegistrationDateString { get { return RegistrationDate.ToString("G"); } }

        public string EnrolleeName { get; set; }

        public string SpecialtyName { get; set; }

        /// <summary>
        /// Флаг, определяющий, если ты заявки у проводавателя не обработанные
        /// </summary>
        public bool IsBusy { get; set; }
    }
}
