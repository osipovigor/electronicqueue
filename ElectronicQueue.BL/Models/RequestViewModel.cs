﻿using ElectronicQueue.BL.Enums;

namespace ElectronicQueue.BL.Models
{
    public class RequestViewModel
    {
        public string RequestId { get; set; }

        public string RequestNumber { get; set; }

        public string EnrolleeLastName { get; set; }

        public string EnrolleeFirstName { get; set; }

        public string SpecialtyName { get; set; }

        public string LectureHallName { get; set; }

        public string LectureTableName { get; set; }

        public string UserFirstName { get; set; }

        public string UserLastName { get; set; }

        public RequestStep StepId { get; set; }
        public string StepName { get; set; }
    }
}
