﻿namespace ElectronicQueue.BL.Enums
{
    /// <summary>
    /// Статус заявки
    /// </summary>
    public enum RequestStep
    {
        New = 1,
        InProcess = 2,
        Closed = 3,
        Accepted = 4
    }
}
