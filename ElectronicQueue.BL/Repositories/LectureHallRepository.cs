﻿using System;
using System.Collections.Generic;
using System.Linq;
using ElectronicQueue.BL.Models;
using ElectronicQueue.Data;
using ElectronicQueue.Data.Entities;

namespace ElectronicQueue.BL.Repositories
{
    public interface ILectureHallRepository : IBaseRepository
    {
        /// <summary>
        /// Получить все столы для отображения во view
        /// </summary>
        /// <returns></returns>
        List<LectureHallTableViewModel> GetAll();

        /// <summary>
        /// Получить все акдитории
        /// </summary>
        /// <returns></returns>
        List<LectureHall> GetLectureHalls();

        /// <summary>
        /// Удалить стол
        /// </summary>
        void DeleteTable(Guid guid);

        /// <summary>
        /// Добавить стол
        /// </summary>
        /// <param name="name"></param>
        /// <param name="lectureHallId"></param>
        /// <param name="specialtyId"></param>
        void AddLectureHallTable(string name, int lectureHallId, Guid specialtyId);

        /// <summary>
        /// 
        /// </summary>
        void ChangeLectureHall(Guid id, int? lectureHallId, Guid specialtyId);

        /// <summary>
        /// Возвращает рандомный стол по специальности
        /// </summary>
        /// <param name="specialtyId"></param>
        /// <returns></returns>
        Guid GetRandomTableBySpeсialty(Guid specialtyId);

        /// <summary>
        /// Получить столы по специальности
        /// </summary>
        /// <param name="specialtyGuid"></param>
        /// <returns></returns>
        List<LectureHallTable> GetLectureHalls(Guid specialtyGuid);
    }
    public class LectureHallRepository : BaseRepository, ILectureHallRepository
    {
        public LectureHallRepository(ElectronicQueueModel model) : base(model)
        {
        }

        public List<LectureHallTableViewModel> GetAll()
        {
            var tables = _model.LectureHallTables.Select(t => new LectureHallTableViewModel
            {
                Id = t.LectureHallTableId.ToString(),
                Name = t.Name,
                LectureHallName = t.LectureHall.Name,
                SpecialtyName = t.Specialty.Name
            });

            return tables.ToList();
        }

        public List<LectureHall> GetLectureHalls()
        {
            return _model.LectureHalls.ToList();
        }

        public void DeleteTable(Guid guid)
        {
            LectureHallTable table = GetLectureHallTable(guid);
            _model.LectureHallTables.Remove(table);
        }

        public void AddLectureHallTable(string name, int lectureHallId, Guid specialtyId)
        {
            LectureHallTable table = new LectureHallTable
            {
                Name = name,
                LectureHall = GetLectureHall(lectureHallId),
                SpecialtyId = specialtyId
            };
            _model.LectureHallTables.Add(table);
        }

        public void ChangeLectureHall(Guid id, int? lectureHallId, Guid specialtyId)
        {
            LectureHallTable table = GetLectureHallTable(id);
            LectureHall lectureHall = GetLectureHall(lectureHallId.Value);
            table.LectureHall = lectureHall;
            table.SpecialtyId = specialtyId;
        }

        public Guid GetRandomTableBySpeсialty(Guid specialtyId)
        {
            LectureHallTable[] result = _model.LectureHallTables.Where(x => x.SpecialtyId == specialtyId).ToArray();
            if(result.Length == 0)
                throw new Exception("По данной специальности нет столов!");
            var rand = new Random();
            var next = rand.Next(0, result.Length);
            return result[next].LectureHallTableId;
        }

        public List<LectureHallTable> GetLectureHalls(Guid specialtyGuid)
        {
            return _model.LectureHallTables.Where(x => x.SpecialtyId == specialtyGuid).ToList();
        }

        private LectureHall GetLectureHall(int id)
        {
            return _model.LectureHalls.FirstOrDefault(x => x.LectureHallId == id);
        }
        private LectureHallTable GetLectureHallTable(Guid guid)
        {
            return _model.LectureHallTables.FirstOrDefault(x => x.LectureHallTableId == guid);
        }
    }
}
