﻿using ElectronicQueue.Data;

namespace ElectronicQueue.BL.Repositories
{
    public interface IBaseRepository
    {
        int SaveChanges();
    }

    public abstract class BaseRepository : IBaseRepository
    {
        protected readonly ElectronicQueueModel _model;

        protected BaseRepository(ElectronicQueueModel model)
        {
            _model = model;
        }
        
        public int SaveChanges()
        {
            return _model.SaveChanges();
        }

    }
}
