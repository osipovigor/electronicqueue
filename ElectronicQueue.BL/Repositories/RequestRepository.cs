﻿using System;
using System.Collections.Generic;
using System.Linq;
using ElectronicQueue.BL.Enums;
using ElectronicQueue.BL.Models;
using ElectronicQueue.Data;
using ElectronicQueue.Data.Entities;

namespace ElectronicQueue.BL.Repositories
{
    public interface IRequestRepository : IBaseRepository
    {
        /// <summary>
        /// Зарегистрировать абитуриента
        /// </summary>
        void RegisterEnrollee(Enrollee enrollee);

        /// <summary>
        /// Создать заявку
        /// </summary>
        void CreateRequest(Request request);

        /// <summary>
        /// Получить список столов аудиторий
        /// </summary>
        /// <returns></returns>
        List<LectureHallTable> GetLectureHallTables();

        /// <summary>
        /// Получить список всех заявок
        /// </summary>
        /// <returns></returns>
        List<RequestViewModel> GetRequests();

        /// <summary>
        /// Получить заявку по номеру
        /// </summary>
        /// <param name="requestNumber"></param>
        /// <returns></returns>
        Request GetRequest(int requestNumber);

        /// <summary>
        /// Получить рандомный номер заявки
        /// </summary>
        /// <returns></returns>
        int GetRequestNumber();

        /// <summary>
        /// Удалить заявку
        /// </summary>
        /// <param name="requestId"></param>
        void DeleteRequest(Guid requestId);

        /// <summary>
        /// Получить заявку определенного преподавателя
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="isChecked"></param>
        /// <returns></returns>
        EnrolleeViewModel GetTopRequest(string userName, bool isChecked);

        /// <summary>
        /// Принять заявку
        /// </summary>
        /// <param name="requestNumber"></param>
        /// <param name="isClosed"></param>
        void ApplyRequest(int requestNumber, bool isClosed);
    }
    public class RequestRepository: BaseRepository, IRequestRepository
    {
        private readonly IUserRepository _userRepository;

        public RequestRepository(ElectronicQueueModel model, IUserRepository userRepository) : base(model)
        {
            _userRepository = userRepository;
        }

        public void RegisterEnrollee(Enrollee enrollee)
        {
            _model.Enrollees.Add(enrollee);
        }

        public void CreateRequest(Request request)
        {
            _model.Requests.Add(request);
        }

        public List<LectureHallTable> GetLectureHallTables()
        {
            return _model.LectureHallTables.ToList();
        }

        public List<RequestViewModel> GetRequests()
        {
            IQueryable<RequestViewModel> requests = from r in _model.Requests
                join e in _model.Enrollees on r.EnrolleeId equals e.EnrolleeId
                join s in _model.Specialties on r.SpecialtyId equals s.SpecialtyId
                join t in _model.LectureHallTables on r.LectureHallTableId equals t.LectureHallTableId
                join u in _model.Users on s.UserId equals u.UserId into us
                from u in us.DefaultIfEmpty()
                orderby r.CreationDate descending
                select new RequestViewModel
                {
                    RequestId = r.RequestId.ToString(),
                    RequestNumber = r.RequestNumber.ToString(),
                    EnrolleeFirstName = e.FirstName,
                    EnrolleeLastName = e.LastName,
                    SpecialtyName = s.Name,
                    LectureHallName = t.LectureHall.Name,
                    LectureTableName = t.Name,
                    UserFirstName = u.FirstName,
                    UserLastName = u.LastName,
                    StepId = (RequestStep) r.StepId,
                    StepName = r.Step.Name
                };

            return requests.ToList();
        }

        public Request GetRequest(int requestNumber)
        {
            return _model.Requests.FirstOrDefault(x => x.RequestNumber == requestNumber);
        }

        public int GetRequestNumber()
        {
            Random rand = new Random();
            const int MIN_VALUE = 1111;
            const int MAX_VALUE = 9999;
            int number = rand.Next(MIN_VALUE, MAX_VALUE);
            while (GetRequest(number) != null)
                number = rand.Next(MIN_VALUE, MAX_VALUE);
            return number;

        }

        public void DeleteRequest(Guid requestId)
        {
            Request request = GetRequest(requestId);
            if (request != null)
                _model.Requests.Remove(request);
        }

        public EnrolleeViewModel GetTopRequest(string userName, bool isChecked)
        {
            User user = _userRepository.GetUser(userName);

            Request currentRequest = GetInStayRequest(user.UserId);
            EnrolleeViewModel result;
            if (currentRequest != null)
            {
                result = (from r in _model.Requests
                          join s in _model.Specialties on r.SpecialtyId equals s.SpecialtyId
                          join e in _model.Enrollees on r.EnrolleeId equals e.EnrolleeId
                          where r.RequestId == currentRequest.RequestId
                    select new EnrolleeViewModel()
                    {
                        RequestNumber = r.RequestNumber,
                        SpecialtyName = s.Name,
                        EnrolleeName = e.FirstName,
                        RegistrationDate = r.CreationDate,
                        IsBusy = true

                    }).FirstOrDefault();
                return result;
            }

            if (isChecked)
                return null;

            result = (from r in _model.Requests
                join s in _model.Specialties on r.SpecialtyId equals s.SpecialtyId
                join e in _model.Enrollees on r.EnrolleeId equals e.EnrolleeId
                where s.UserId == user.UserId && r.StepId == (int) RequestStep.New
                orderby r.CreationDate descending
                select new EnrolleeViewModel
                {
                    RequestNumber = r.RequestNumber,
                    SpecialtyName = s.Name,
                    EnrolleeName = e.FirstName,
                    RegistrationDate = r.CreationDate,
                    IsBusy = false
                }).FirstOrDefault();

            

            if(result == null)
                throw new Exception("У вас нет заявок");
            Request request = GetRequest(result.RequestNumber);
            request.StepId = (int) RequestStep.InProcess;
            SaveChanges();

            return result;
        }

        public void ApplyRequest(int requestNumber, bool isClosed)
        {
            Request request = GetRequest(requestNumber);
            if (isClosed)
                request.StepId = (int) RequestStep.Closed;
            else
                request.StepId = (int) RequestStep.Accepted;
            request.ClosingDate = DateTime.Now;;
        }

        /// <summary>
        /// Получить необработанную заявку
        /// </summary>
        /// <param name="userGuid"></param>
        /// <returns></returns>
        private Request GetInStayRequest(Guid userGuid)
        {

            IQueryable<Request> result = from r in _model.Requests
                join s in _model.Specialties on r.SpecialtyId equals s.SpecialtyId
                join e in _model.Enrollees on r.EnrolleeId equals e.EnrolleeId
                where
                    s.UserId == userGuid &&r.StepId == (int) RequestStep.InProcess
                select r;
            return result.FirstOrDefault();
        }

        private Request GetRequest(Guid requestId)
        {
            return _model.Requests.FirstOrDefault(x => x.RequestId == requestId);
        }
    }
}
