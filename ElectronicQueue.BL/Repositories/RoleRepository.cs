﻿using System.Linq;
using ElectronicQueue.Data;
using ElectronicQueue.Data.Entities;

namespace ElectronicQueue.BL.Repositories
{
    public interface IRoleRepository : IBaseRepository
    {
        /// <summary>
        /// Получить роль по ID
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        Role GetRole(int roleId);

        /// <summary>
        /// Добавить новую роль
        /// </summary>
        /// <param name="newRole"></param>
        void AddRole(Role newRole);
    }

    public class RoleRepository : BaseRepository, IRoleRepository
    {
        public RoleRepository(ElectronicQueueModel model) : base(model)
        {
        }

        public Role GetRole(int roleId)
        {
            return _model.Roles.FirstOrDefault(x => x.RoleId == roleId);
        }

        public void AddRole(Role newRole)
        {
            _model.Roles.Add(newRole);
        }
    }
}
