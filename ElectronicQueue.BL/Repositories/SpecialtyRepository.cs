﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using ElectronicQueue.BL.Models;
using ElectronicQueue.Data;
using ElectronicQueue.Data.Configurations;
using ElectronicQueue.Data.Entities;

namespace ElectronicQueue.BL.Repositories
{
    public interface ISpecialtyRepository : IBaseRepository
    {
        /// <summary>
        /// Метод возвращает список специальностей
        /// </summary>
        /// <returns></returns>
        List<Specialty> GetAll();

        /// <summary>
        /// Метод возвращает специальность по ГУИДу
        /// </summary>
        /// <returns></returns>
        Specialty Get(Guid guid);

        /// <summary>
        /// Метод удаляет специальность
        /// </summary>
        /// <param name="guid">ГУИД специальности</param>
        void Delete(string guid);

        /// <summary>
        /// Метод добавляет специальность
        /// </summary>
        /// <param name="name"></param>
        /// <param name="isPrior"></param>
        /// <param name="teacher"></param>
        void Add(string name, bool isPrior, string teacher);

        /// <summary>
        /// Список специальностей для отображения в ПУ
        /// </summary>
        /// <returns></returns>
        List<SpecialtyViewModel> GetSpecialtiesForControlPanel();

        /// <summary>
        /// Получить строку со списком специальностей
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        string GetSpecialtyNames(string name);

        /// <summary>
        /// Изменить специальность
        /// </summary>
        /// <param name="specialtyId"></param>
        /// <param name="name"></param>
        /// <param name="userId"></param>
        void ChangeSpecialty(string specialtyId, string name, string userId);
    }

    public class SpecialtyRepository : BaseRepository, ISpecialtyRepository
    {
        private readonly ILectureHallRepository _lectureHallRepository;

        public SpecialtyRepository(ElectronicQueueModel model, ILectureHallRepository lectureHallRepository) : base(model)
        {
            _lectureHallRepository = lectureHallRepository;
        }


        public List<Specialty> GetAll()
        {
            return _model.Specialties.ToList();
        }

        public Specialty Get(Guid guid)
        {
            return _model.Specialties.FirstOrDefault(x => x.SpecialtyId == guid);
        }

        public void Delete(string guid)
        {
            List<LectureHallTable> tables = _lectureHallRepository.GetLectureHalls(Guid.Parse(guid));

            foreach (LectureHallTable table in tables)
            {
                table.SpecialtyId = null;
                _model.LectureHallTables.AddOrUpdate(table);
            }
                

            //удаляем специальность
            Specialty specialty = Get(Guid.Parse(guid));
            _model.Specialties.Remove(specialty);
        }

        public void Add(string name, bool isPrior, string userId)
        {
            Specialty specialty = new Specialty { Name = name, IsPrior = isPrior, UserId = Guid.Parse(userId)};
            _model.Specialties.Add(specialty);
        }

        public List<SpecialtyViewModel> GetSpecialtiesForControlPanel()
        {
            IQueryable<SpecialtyViewModel> result = from s in _model.Specialties
                join u in _model.Users on s.UserId equals u.UserId into us
                from u in us.DefaultIfEmpty()
                select new SpecialtyViewModel()
                {
                    SpecialtyId = s.SpecialtyId.ToString(),
                    Name = s.Name,
                    Teacher = u == null ? "Noname" : u.LastName
                };
            return result.ToList();
        }

        public string GetSpecialtyNames(string name)
        {
            IQueryable<string> query = from s in _model.Specialties
                join u in _model.Users on s.UserId equals u.UserId
                where u.Login == name
                select s.Name;

            return string.Join(", ", query.ToArray());
        }

        public void ChangeSpecialty(string specialtyId, string name, string userId)
        {
            Specialty specialty = Get(Guid.Parse(specialtyId));
            specialty.Name = string.IsNullOrEmpty(name) ? string.Empty : name;
            specialty.UserId = Guid.Parse(userId);
        }
    }
}