﻿using System;
using System.Collections.Generic;
using System.Linq;
using ElectronicQueue.BL.Models;
using ElectronicQueue.Data;
using ElectronicQueue.Data.Entities;

namespace ElectronicQueue.BL.Repositories
{
    public interface IUserRepository : IBaseRepository
    {
        /// <summary>
        /// Получить пользователя по логину/паролю
        /// </summary>
        User GetUser(string username);

        /// <summary>
        /// Добавление пользователя в бд
        /// </summary>
        void AddUser(User user);

        /// <summary>
        /// Возвращает список всех пользователей, для оторбражения операторов в панели управления
        /// </summary>
        /// <returns></returns>
        List<OperatorsViewModel> GetUsers();

        /// <summary>
        /// Удаляет пользователя
        /// </summary>
        /// <param name="guid"></param>
        void DeleteUser(Guid guid);

        /// <summary>
        /// Получить список преподавателей
        /// </summary>
        /// <returns></returns>
        List<User> GetTeachers();
    }

    public class UserRepository: BaseRepository, IUserRepository
    {
        public UserRepository(ElectronicQueueModel model) : base(model)
        {
        }


        public User GetUser(string username)
        {
            User user = _model.Users.FirstOrDefault(x => x.Login == username);
            return user;
        }

        public void AddUser(User user)
        {
            User currentUser = GetUser(user.Login);
            if(currentUser != null)
                throw new Exception("Такой пользователь уже существует");
            _model.Users.Add(user);
        }

        public List<OperatorsViewModel> GetUsers()
        {
            IQueryable<OperatorsViewModel> result = from u in _model.Users
                join r in _model.Roles on u.RoleId equals r.RoleId
                select new OperatorsViewModel
                {
                    UserId = u.UserId,
                    Login = u.Login,
                    Password = u.Password,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    RoleName = r.RoleName
                };

            return result.ToList();
        }

        public void DeleteUser(Guid guid)
        {
            User user = GetUser(guid);
            _model.Users.Remove(user);
        }

        public List<User> GetTeachers()
        {
            const int TEACHER_ROLE = 3;
            return _model.Users.Where(x => x.RoleId == TEACHER_ROLE).ToList();
        }

        private User GetUser(Guid guid)
        {
            return _model.Users.FirstOrDefault(x => x.UserId == guid);
        }
    }
}
