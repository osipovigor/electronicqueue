using ElectronicQueue.Data.Configurations;
using ElectronicQueue.Data.Entities;
using System.Data.Entity;


namespace ElectronicQueue.Data
{
    public class ElectronicQueueModel : DbContext
    {
        public ElectronicQueueModel() : base("name=ElectronicQueueModel") { }
        
        public DbSet<Enrollee> Enrollees { get; set; }
        public DbSet<LectureHall> LectureHalls { get; set; }
        public DbSet<LectureHallTable> LectureHallTables { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Specialty> Specialties { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Step> Steps { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new EnrolleeConfiguration());
            modelBuilder.Configurations.Add(new LectureHallConfiguration());
            modelBuilder.Configurations.Add(new LectureHallTableConfiguration());
            modelBuilder.Configurations.Add(new RequestConfiguration());
            modelBuilder.Configurations.Add(new SpecialtyConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
           
        }
    }
    
}