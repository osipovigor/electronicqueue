﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElectronicQueue.Data.Entities
{
    /// <summary>
    /// Аудитория
    /// </summary>
    public class LectureHall
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LectureHallId { get; set; }

        public string Name { get; set; }
        
        public virtual ICollection<LectureHallTable> LectureHallTables { get; set; } 

        
    }
}
