﻿using System.Collections.Generic;

namespace ElectronicQueue.Data.Entities
{
    public class Step
    {
        public int StepId { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Request> Request { get; set; }
    }
}
