﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElectronicQueue.Data.Entities
{
    /// <summary>
    /// Стол
    /// </summary>
    public class LectureHallTable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid LectureHallTableId { get; set; }

        public string Name { get; set; }

        public virtual LectureHall LectureHall { get; set; }
        
        public virtual int LectureHallId { get; set; }

        public virtual ICollection<Request> Requests { get; set; }
       
        public Guid? SpecialtyId { get; set; } 

        public Specialty Specialty { get; set; }
    }
}
