﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElectronicQueue.Data.Entities
{
    public class Request
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid RequestId { get; set; }
        
        public DateTime CreationDate { get; set; }

        public DateTime? ClosingDate { get; set; }

        public int RequestNumber { get; set; }

        public virtual Guid EnrolleeId { get; set; }
        public virtual Enrollee Enrollee { get; set; }

        public virtual Guid SpecialtyId { get; set; }
        public virtual Specialty Specialty { get; set; }

        public virtual Guid LectureHallTableId { get; set; }
        public virtual LectureHallTable LectureHallTable { get; set; }

        public virtual Step Step { get; set; }
        public virtual int StepId { get; set; }
        
    }
}
