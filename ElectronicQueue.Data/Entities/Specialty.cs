﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElectronicQueue.Data.Entities
{
    public class Specialty
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid SpecialtyId { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Определяет, приоритетная ли специальность
        /// </summary>
        public bool IsPrior { get; set; }

        public virtual ICollection<Request> Requests { get; set; }

        public virtual ICollection<LectureHallTable> Tables { get; set; }

        public Guid? UserId { get; set; }

        public User User { get; set; }
    }
}
