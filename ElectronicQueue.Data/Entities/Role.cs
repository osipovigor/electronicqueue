﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElectronicQueue.Data.Entities
{
    public class Role
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RoleId { get; set; }

        public string RoleName { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
