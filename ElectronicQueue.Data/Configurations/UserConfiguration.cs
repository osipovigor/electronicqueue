﻿using System.Data.Entity.ModelConfiguration;
using ElectronicQueue.Data.Entities;

namespace ElectronicQueue.Data.Configurations
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            Property(x => x.Login).IsRequired().HasMaxLength(60);
            Property(x => x.Password).IsRequired().HasMaxLength(60);
        }
    }
}
