﻿using System.Data.Entity.ModelConfiguration;
using ElectronicQueue.Data.Entities;

namespace ElectronicQueue.Data.Configurations
{
    public class LectureHallTableConfiguration : EntityTypeConfiguration<LectureHallTable>
    {
        public LectureHallTableConfiguration()
        {
            Property(c => c.Name).IsRequired().HasMaxLength(40);
        }
    }
}
