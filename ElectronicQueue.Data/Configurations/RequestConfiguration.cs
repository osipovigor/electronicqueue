﻿using System.Data.Entity.ModelConfiguration;
using ElectronicQueue.Data.Entities;

namespace ElectronicQueue.Data.Configurations
{
    public class RequestConfiguration : EntityTypeConfiguration<Request>
    {
        public RequestConfiguration()
        {
            Property(c => c.LectureHallTableId).IsRequired();
            Property(c => c.EnrolleeId).IsRequired();
            Property(c => c.SpecialtyId).IsRequired();
            Property(c => c.CreationDate).IsRequired();
            Property(c => c.RequestNumber).IsRequired();
        }
    }
}
