﻿using System.Data.Entity.ModelConfiguration;
using ElectronicQueue.Data.Entities;

namespace ElectronicQueue.Data.Configurations
{
    public class LectureHallConfiguration : EntityTypeConfiguration<LectureHall>
    {
        public LectureHallConfiguration()
        {
            Property(c => c.Name).IsRequired().HasMaxLength(50);
        }
    }
}
