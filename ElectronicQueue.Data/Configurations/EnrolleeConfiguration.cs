﻿using System.Data.Entity.ModelConfiguration;
using ElectronicQueue.Data.Entities;

namespace ElectronicQueue.Data.Configurations
{
    public class EnrolleeConfiguration : EntityTypeConfiguration<Enrollee>
    {
        public EnrolleeConfiguration()
        {
            Property(c => c.FirstName).IsRequired().HasMaxLength(30);
            Property(c => c.LastName).IsRequired().HasMaxLength(30);
            Property(c => c.CreationDate).IsRequired();
        }
    }
}
