﻿using System.Data.Entity.ModelConfiguration;
using ElectronicQueue.Data.Entities;

namespace ElectronicQueue.Data.Configurations
{
    public class SpecialtyConfiguration : EntityTypeConfiguration<Specialty>
    {
        public SpecialtyConfiguration()
        {
            Property(c => c.Name).IsRequired();
            Property(c => c.IsPrior).IsRequired();
            HasOptional(s => s.User)
                .WithMany(x => x.Specialties)
                .WillCascadeOnDelete(true);

        }
    }
}
