namespace ElectronicQueue.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteLectureHallTableIdFromLectureHallTable : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.LectureHallTables", "LectureHallId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LectureHallTables", "LectureHallId", c => c.Guid(nullable: false));
        }
    }
}
