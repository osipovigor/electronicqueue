namespace ElectronicQueue.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Enrollees",
                c => new
                    {
                        EnrolleeId = c.Guid(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 30),
                        LastName = c.String(nullable: false, maxLength: 30),
                        CreationDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.EnrolleeId);
            
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        RequestId = c.Guid(nullable: false),
                        EnrolleeId = c.Guid(nullable: false),
                        SpecialtyId = c.Guid(nullable: false),
                        LectureHallTableId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.RequestId)
                .ForeignKey("dbo.Enrollees", t => t.EnrolleeId, cascadeDelete: true)
                .ForeignKey("dbo.LectureHallTables", t => t.LectureHallTableId, cascadeDelete: true)
                .ForeignKey("dbo.Specialties", t => t.SpecialtyId, cascadeDelete: true);
            
            CreateTable(
                "dbo.LectureHallTables",
                c => new
                    {
                        LectureHallTableId = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 40),
                        LectureHallId = c.Guid(nullable: false),
                        LectureHall_LectureHallId = c.Int(),
                    })
                .PrimaryKey(t => t.LectureHallTableId)
                .ForeignKey("dbo.LectureHalls", t => t.LectureHall_LectureHallId);
            
            CreateTable(
                "dbo.LectureHalls",
                c => new
                    {
                        LectureHallId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.LectureHallId);
            
            CreateTable(
                "dbo.Specialties",
                c => new
                    {
                        SpecialtyId = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        IsPrior = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SpecialtyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Requests", "SpecialtyId", "dbo.Specialties");
            DropForeignKey("dbo.Requests", "LectureHallTableId", "dbo.LectureHallTables");
            DropForeignKey("dbo.LectureHallTables", "LectureHall_LectureHallId", "dbo.LectureHalls");
            DropForeignKey("dbo.Requests", "EnrolleeId", "dbo.Enrollees");
            DropTable("dbo.Specialties");
            DropTable("dbo.LectureHalls");
            DropTable("dbo.LectureHallTables");
            DropTable("dbo.Requests");
            DropTable("dbo.Enrollees");
        }
    }
}
