namespace ElectronicQueue.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CascadeOnDeleteSpecialtyUser : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Specialties", "UserId", "dbo.Users");
            AddForeignKey("dbo.Specialties", "UserId", "dbo.Users", "UserId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Specialties", "UserId", "dbo.Users");
            AddForeignKey("dbo.Specialties", "UserId", "dbo.Users", "UserId");
        }
    }
}
