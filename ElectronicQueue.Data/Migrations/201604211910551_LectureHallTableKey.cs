namespace ElectronicQueue.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LectureHallTableKey : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Requests", "RequestId", c => c.Guid(nullable: false, identity: true, defaultValueSql:"newid()"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Requests", "RequestId", c => c.Guid(nullable: false, identity: true, defaultValueSql: null));
        }
    }
}
