namespace ElectronicQueue.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeIdType : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.LectureHallTables", new[] { "User_UserId" });
            DropColumn("dbo.LectureHallTables", "UserId");
            RenameColumn(table: "dbo.LectureHallTables", name: "User_UserId", newName: "UserId");
            AlterColumn("dbo.LectureHallTables", "UserId", c => c.Guid());
            CreateIndex("dbo.LectureHallTables", "UserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.LectureHallTables", new[] { "UserId" });
            AlterColumn("dbo.LectureHallTables", "UserId", c => c.Int());
            RenameColumn(table: "dbo.LectureHallTables", name: "UserId", newName: "User_UserId");
            AddColumn("dbo.LectureHallTables", "UserId", c => c.Int());
            CreateIndex("dbo.LectureHallTables", "User_UserId");
        }
    }
}
