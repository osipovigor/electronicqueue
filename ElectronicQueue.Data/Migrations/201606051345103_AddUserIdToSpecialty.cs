namespace ElectronicQueue.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserIdToSpecialty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Specialties", "UserId", c => c.Guid());
            CreateIndex("dbo.Specialties", "UserId");
            AddForeignKey("dbo.Specialties", "UserId", "dbo.Users", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Specialties", "UserId", "dbo.Users");
            DropIndex("dbo.Specialties", new[] { "UserId" });
            DropColumn("dbo.Specialties", "UserId");
        }
    }
}
