namespace ElectronicQueue.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRequestNumberToRequests : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Requests", "RequestNumber", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Requests", "RequestNumber");
        }
    }
}
