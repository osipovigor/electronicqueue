namespace ElectronicQueue.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_lecture_hall_id : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.LectureHallTables", "LectureHall_LectureHallId", "dbo.LectureHalls");
            DropIndex("dbo.LectureHallTables", new[] { "LectureHall_LectureHallId" });
            RenameColumn(table: "dbo.LectureHallTables", name: "LectureHall_LectureHallId", newName: "LectureHallId");
            AlterColumn("dbo.LectureHallTables", "LectureHallId", c => c.Int(nullable: false));
            CreateIndex("dbo.LectureHallTables", "LectureHallId");
            AddForeignKey("dbo.LectureHallTables", "LectureHallId", "dbo.LectureHalls", "LectureHallId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LectureHallTables", "LectureHallId", "dbo.LectureHalls");
            DropIndex("dbo.LectureHallTables", new[] { "LectureHallId" });
            AlterColumn("dbo.LectureHallTables", "LectureHallId", c => c.Int());
            RenameColumn(table: "dbo.LectureHallTables", name: "LectureHallId", newName: "LectureHall_LectureHallId");
            CreateIndex("dbo.LectureHallTables", "LectureHall_LectureHallId");
            AddForeignKey("dbo.LectureHallTables", "LectureHall_LectureHallId", "dbo.LectureHalls", "LectureHallId");
        }
    }
}
