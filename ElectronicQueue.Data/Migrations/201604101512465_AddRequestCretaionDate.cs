namespace ElectronicQueue.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRequestCretaionDate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Requests", "EnrolleeId", "dbo.Enrollees");
            DropForeignKey("dbo.Requests", "LectureHallTableId", "dbo.LectureHallTables");
            DropPrimaryKey("dbo.Enrollees");
            DropPrimaryKey("dbo.Requests");
            DropPrimaryKey("dbo.LectureHallTables");
            AddColumn("dbo.Requests", "CreationDate", c => c.DateTime(nullable: false, defaultValue: DateTime.Now));
            AlterColumn("dbo.Enrollees", "EnrolleeId", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.Requests", "RequestId", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.LectureHallTables", "LectureHallTableId", c => c.Guid(nullable: false, identity: true));
            AddPrimaryKey("dbo.Enrollees", "EnrolleeId");
            AddPrimaryKey("dbo.Requests", "RequestId");
            AddPrimaryKey("dbo.LectureHallTables", "LectureHallTableId");
            AddForeignKey("dbo.Requests", "EnrolleeId", "dbo.Enrollees", "EnrolleeId", cascadeDelete: true);
            AddForeignKey("dbo.Requests", "LectureHallTableId", "dbo.LectureHallTables", "LectureHallTableId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Requests", "LectureHallTableId", "dbo.LectureHallTables");
            DropForeignKey("dbo.Requests", "EnrolleeId", "dbo.Enrollees");
            DropPrimaryKey("dbo.LectureHallTables");
            DropPrimaryKey("dbo.Requests");
            DropPrimaryKey("dbo.Enrollees");
            AlterColumn("dbo.LectureHallTables", "LectureHallTableId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Requests", "RequestId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Enrollees", "EnrolleeId", c => c.Guid(nullable: false));
            DropColumn("dbo.Requests", "CreationDate");
            AddPrimaryKey("dbo.LectureHallTables", "LectureHallTableId");
            AddPrimaryKey("dbo.Requests", "RequestId");
            AddPrimaryKey("dbo.Enrollees", "EnrolleeId");
            AddForeignKey("dbo.Requests", "LectureHallTableId", "dbo.LectureHallTables", "LectureHallTableId", cascadeDelete: true);
            AddForeignKey("dbo.Requests", "EnrolleeId", "dbo.Enrollees", "EnrolleeId", cascadeDelete: true);
        }
    }
}
