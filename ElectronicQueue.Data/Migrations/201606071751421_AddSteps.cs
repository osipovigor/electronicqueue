namespace ElectronicQueue.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSteps : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Steps",
                c => new
                    {
                        StepId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.StepId);
            
            AddColumn("dbo.Requests", "ClosingDate", c => c.DateTime());
            AddColumn("dbo.Requests", "StepId", c => c.Int(nullable: false));
            CreateIndex("dbo.Requests", "StepId");
            AddForeignKey("dbo.Requests", "StepId", "dbo.Steps", "StepId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Requests", "StepId", "dbo.Steps");
            DropIndex("dbo.Requests", new[] { "StepId" });
            DropColumn("dbo.Requests", "StepId");
            DropColumn("dbo.Requests", "ClosingDate");
            DropTable("dbo.Steps");
        }
    }
}
