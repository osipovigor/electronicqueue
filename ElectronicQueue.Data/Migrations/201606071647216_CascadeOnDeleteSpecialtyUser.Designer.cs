// <auto-generated />
namespace ElectronicQueue.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class CascadeOnDeleteSpecialtyUser : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CascadeOnDeleteSpecialtyUser));
        
        string IMigrationMetadata.Id
        {
            get { return "201606071647216_CascadeOnDeleteSpecialtyUser"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
