using ElectronicQueue.Data.Entities;

namespace ElectronicQueue.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<Data.ElectronicQueueModel>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ElectronicQueue.Data.ElectronicQueueModel";
        }

        protected override void Seed(ElectronicQueueModel context)
        {
            context.Roles.AddOrUpdate(x => x.RoleName, 
                new Role { RoleName = "admin" },
                new Role { RoleName = "operator" },
                new Role { RoleName = "teacher" }
                );

            context.Steps.AddOrUpdate(x => x.Name,
                new Step { Name = "�����" },
                new Step { Name = "� ��������" },
                new Step { Name = "�������" },
                new Step { Name = "������" }
                );
        }
    }
}
