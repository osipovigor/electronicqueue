namespace ElectronicQueue.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserTablesRelations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LectureHallTables", "UserId", c => c.Int());
            AddColumn("dbo.LectureHallTables", "User_UserId", c => c.Guid());
            CreateIndex("dbo.LectureHallTables", "User_UserId");
            AddForeignKey("dbo.LectureHallTables", "User_UserId", "dbo.Users", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LectureHallTables", "User_UserId", "dbo.Users");
            DropIndex("dbo.LectureHallTables", new[] { "User_UserId" });
            DropColumn("dbo.LectureHallTables", "User_UserId");
            DropColumn("dbo.LectureHallTables", "UserId");
        }
    }
}
